require 'mini_magick'

class ImageProcessor

  def get_composed_image_path

      beginning = Time.now

      #set GraphicsMagick as processor
      MiniMagick.processor = :gm
      MiniMagick.validate_on_create = false
      MiniMagick.validate_on_write = false

      #background
      background = MiniMagick::Image.open('lib/assets/images/algemeen-thumbnails/achtergronden/achtergrond01.jpg')

      #add furniture
      sh_mirror = MiniMagick::Image.open("lib/assets/images/MADDOX/spiegel/spiegel__illum0013.jpg")
      sh_middle = MiniMagick::Image.open("lib/assets/images/MADDOX/onderkast/onderkast__illum0009.jpg")
      sh_right = MiniMagick::Image.open("lib/assets/images/MADDOX/kolomkast-r/kolomkast_r__illum0013.jpg")
      sh_left = MiniMagick::Image.open("lib/assets/images/MADDOX/kolomkast-l/kolomkast_l__illum0013.jpg")

      furnitures = [sh_mirror, sh_middle, sh_left, sh_right]

      furnitures.each do |furniture|
        background = background.composite(furniture) do |c|
          c.compose 'multiply'
          if furniture == sh_left
            c.geometry '-500'
          elsif furniture == sh_right
            c.geometry '+1230'
          end
        end
      end


      #add colors
      cl_mirror = MiniMagick::Image.open("lib/assets/images/MADDOX/spiegel/spiegel_color_0013.png")
      cl_middle = MiniMagick::Image.open("lib/assets/images/MADDOX/onderkast/onderkast_color_0009.png")
      cl_right = MiniMagick::Image.open("lib/assets/images/MADDOX/kolomkast-r/kolomkast_r_color_0013.png")
      cl_left = MiniMagick::Image.open("lib/assets/images/MADDOX/kolomkast-l/kolomkast_l_color_0013.png")
      

      colors = [cl_mirror, cl_middle, cl_left, cl_right]

      colors.each do |color|
        background = background.composite(color) do |c|
          c.compose 'over'
          if color == cl_left
            c.geometry '-500'
          elsif color == cl_right
            c.geometry '+1800'
          end
        end
      end


      background.write('app/assets/images/mini_magick_result.jpg')

      ending = Time.now
      elapsed_time = ending - beginning
      puts "elapsed time is " + elapsed_time.to_s + "seconds"

      return "mini_magick_result.jpg"
  end

end