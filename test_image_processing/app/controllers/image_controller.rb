require 'image_processor'

class ImageController < ApplicationController

  def get_image
    img_p = ImageProcessor.new
    @image_path = img_p.get_composed_image_path
    
    render "image/image_page"
  end

end
